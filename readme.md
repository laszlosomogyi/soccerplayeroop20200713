#SoccerPlayer

##adattagok:
- name
- passAttempts
- passes
- shots
- goal
- price

##metódusok:
- passEfficiency: eredményes passzok / passzok
- shotEfficiency: gólok / kapuralövések 
- Effectiveness: (passEfficiency + (shotEfficiency * 2)) / 2
- costEfficiency: Effectiveness / Price
- order by cost efficiency
- most cost efficient
- least cost efficient
    