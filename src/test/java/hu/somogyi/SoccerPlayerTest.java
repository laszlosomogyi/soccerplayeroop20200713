package hu.somogyi;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class SoccerPlayerTest {

    @Nested
    @DisplayName("Pass Efficiency of a Player")
    public class PassEfficiencyTests {

        @Test
        @DisplayName("with 10 passes and 5 successful passes is 0.5")
        public void PassEfficiency() {
            Player player1 = new Player(10, 5);
            double expectedPassEfficiency = 0.5;
            assertEquals(expectedPassEfficiency, player1.passEfficiency(), 0);
        }

        @Test
        public void PassEfficiencyZeroPasses() {
            Player player1 = new Player(0, 0);
            double expectedPassEfficiency = 0.0;
            assertEquals(expectedPassEfficiency, player1.passEfficiency(), 0);
        }

        @Test
        public void PassEfficiencyZeroSuccessfulPasses() {
            Player player1 = new Player(10, 0);
            double expectedPassEfficiency = 0.0;
            assertEquals(expectedPassEfficiency, player1.passEfficiency(), 0);
        }

        @Test
        public void PassEfficiencyIfNoOfPassesIsLowerThanNoOfSuccessfulPasses(){
            Player player1 = new Player(1, 5);
            double expectedPassEfficiency = -1.0;
            assertEquals(expectedPassEfficiency, player1.passEfficiency(), 0);
        }

    }

    @Test
    public void shotEfficiency() {
        Player player1 = new Player(10, 10, 10, 5);
        double expectedShotEfficiency = 0.5;
        assertEquals(expectedShotEfficiency, player1.shotEfficiency(), 0);
    }    
    
    @Test
    public void shotEfficiencyZeroShots() {
        Player player1 = new Player(10, 10, 0, 0);
        double expectedShotEfficiency = 0.0;
        assertEquals(expectedShotEfficiency, player1.shotEfficiency(), 0);
    }    
    
    @Test
    public void shotEfficiencyZeroGoals() {
        Player player1 = new Player(1, 1, 1, 0);
        double expectedShotEfficiency = 0.0;
        assertEquals(expectedShotEfficiency, player1.shotEfficiency(), 0);
    }
    
    @Test
    public void shotEfficiencyNoOfShotsIsLowerThanNoOfGoals() {
        Player player1 = new Player(1, 1, 1, 5);
        double expectedShotEfficiency = -1.0;
        assertEquals(expectedShotEfficiency, player1.shotEfficiency(), 0);
    }
    
    @Test
    public void effectiveness() {
        Player player1 = new Player(10, 5, 10, 2);
        double expectedEffectiveness = 0.45;
        assertEquals(expectedEffectiveness, player1.Effectiveness(), 0);
    }
    
    @Test
    public void effectivenessIfNoOfPassesIsLowerThanNoOfSuccessfulPasses() {
        Player player1 = new Player(3, 5, 10, 2);
        double expectedEffectiveness = -1.0;
        assertEquals(expectedEffectiveness, player1.Effectiveness(), 0);
    }
    
    @Test
    public void effectivenessIfNoOfShotsIsLowerThanNoOfGoals() {
        Player player1 = new Player(5, 2, 1, 2);
        double expectedEffectiveness = -1.0;
        assertEquals(expectedEffectiveness, player1.Effectiveness(), 0);
    }
    
    @Test
    public void costEfficiency() {
        Player player1 = new Player(5, 2, 10, 3, 1000000);
        double expectedCostEfficiency = 5.0E-7;
        assertEquals(expectedCostEfficiency, player1.costEfficiency(), 0);
    }
    
    @Test
    public void costEfficiencyIfEffectivenessIsMinusOne() {
        //tests costEfficiency if effectiveness is -1.0
        Player player1 = new Player(2, 5, 10, 3, 1000000);
        double expectedCostEfficiency = -1.0;
        assertEquals(expectedCostEfficiency, player1.costEfficiency(), 0);
    }
    
    @Test
    public void orderByCostEfficiency() {
        Player[] playerArray = new Player[5];
        playerArray[0] = new Player(5, 2, 8, 2, 1000000);
        playerArray[1] = new Player(10, 4, 15, 3, 800000);
        playerArray[2] = new Player(8, 5, 12, 5, 3000000);
        playerArray[3] = new Player(9, 7, 11, 4, 1200000);
        playerArray[4] = new Player(3, 2, 7, 1, 1300000);
        List<Player> players = Arrays.asList(playerArray);

        Player[] expectedOrderedPlayersArray = {playerArray[3], playerArray[1], playerArray[0], playerArray[4], playerArray[2]};
        List<Player> expectedOrderedPlayers = Arrays.asList(expectedOrderedPlayersArray);

        Collections.sort(players);

        assertArrayEquals(expectedOrderedPlayers.toArray(), players.toArray());
    }
    
}
