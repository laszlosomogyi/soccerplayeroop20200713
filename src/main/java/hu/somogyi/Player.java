
package hu.somogyi;

class Player implements Comparable<Player> {
    int passes;
    int successfulPasses;
    int shots;
    int goals;
    int price;
    
    Player(int passes, int successfulPasses) {
        this.passes = passes;
        this.successfulPasses = successfulPasses;
    }

    Player(int passes, int successfulPasses, int shots, int goals) {
        this.passes = passes;
        this.successfulPasses = successfulPasses;
        this.shots = shots;
        this.goals = goals;
    }

    Player(int passes, int successfulPasses, int shots, int goals, int price) {
        this.passes = passes;
        this.successfulPasses = successfulPasses;
        this.shots = shots;
        this.goals = goals;
        this.price = price;
    }

    Player() {
    }
    
    public double passEfficiency() {
        if (passes == 0) {
            System.out.println("No passes initiated, thus we cannot calculate Pass Efficiency");
            return 0.0;
        } else if(passes < successfulPasses) {
            System.out.println("No. of successful passes cannot exceed No. of passes");
            return -1.0;
        } else {
            return successfulPasses * 1.0 / passes;        
        }
    }

    public double shotEfficiency() {
        if (shots == 0) {
            System.out.println("No shots initiated, thus we cannot calculate shot Efficiency");
            return 0.0;
        } else if(shots < goals) {
            System.out.println("No. of goals cannot exceed No. of shots");
            return -1.0;
        } else {
            return goals * 1.0 /shots;        
        }
    }

    double Effectiveness() {
        if (passEfficiency() == -1.0 || shotEfficiency() == -1.0) {
            return -1.0;
        } else {
        return (passEfficiency() + (shotEfficiency() * 2)) / 2;            
        }
    }

    double costEfficiency() {
        if (Effectiveness() == -1.0) {
            return -1.0;
        } else {
            return Effectiveness()/price;        
        }
    }

    @Override
    public int compareTo(Player o) {
        double difference = o.costEfficiency() - costEfficiency();
        if(difference == 0) return 0;
        else if(difference < 0) return -1;
        else return 1;
    }

    @Override
    public String toString() {
        return "Player(" + passes + "," + successfulPasses + "," + shots + "," + goals + "," + price + ")";
    }
    
    public Player[] orderedPlayers(Player[] playerArray) {
        for (int i = 0; i < playerArray.length; i++) {
            for (int j = 0; j < playerArray.length -1 -i; j++) {
                if (playerArray[j].costEfficiency()>playerArray[j+1].costEfficiency()) {
                    Player tmp = playerArray[j];
                    playerArray[j] = playerArray[j+1];
                    playerArray[j+1] = tmp;
		}
            }
        }
        //orderedPlayers = playerArray;
        return playerArray;
    }
 
}
